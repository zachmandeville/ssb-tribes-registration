const test = require('tape')
const { promisify: p } = require('util')

const { Server, mock, replicate } = require('./helpers')

test('registration.tribe.update', async t => {
  const alice = Server()
  const kaitiaki = Server()

  const groupId = mock.groupId()
  const answers = [
    { q: 'what is your favourate pizza flavour', a: 'hawaiian' }
  ]
  const comment = "p.s. I'm also into adding chilli to hawaiin!"
  const recps = [kaitiaki.id, alice.id]

  let id, val
  try {
    id = await p(alice.registration.tribe.create)(groupId, { answers, recps })
    const updateId = await p(alice.registration.tribe.update)(id, { comment })
    val = await p(alice.get)({ id: updateId, private: true })
  } catch (err) {
    t.fail(err)
  }

  t.deepEqual(
    val.content,
    {
      type: 'registration/group',
      comment: { set: comment },
      tangles: {
        registration: { root: id, previous: [id] }
      },
      recps
    },
    'original applicant can update'
  )

  /* alice cannot approve */
  const decision = { accepted: true }
  let res
  try {
    res = await p(alice.registration.tribe.update)(id, { decision })
  } catch (err) {
    t.match(err.message, /Invalid update message/, 'applicant cannot publish decision')
  }
  if (res) t.fail('alice should not be allowed to decide!')

  /* kaitiaki can approve */
  try {
    await p(replicate)({ from: alice, to: kaitiaki })
    res = await p(kaitiaki.registration.tribe.update)(id, { decision })
  } catch (err) {
    t.fail(err)
  }

  t.true(res, 'kaitiaki can publish decision')

  alice.close()
  kaitiaki.close()
  t.end()
})
