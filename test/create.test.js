const test = require('tape')
const { promisify: p } = require('util')

const { Server } = require('./helpers')

// const sleep = async (t) => new Promise(resolve => setTimeout(resolve, t))
test('registration.tribe.create (v1)', async t => {
  const server = Server()
  const { groupId, poBoxId } = await p(server.tribes.create)({ addPOBox: true })
  const answers = [
    { q: 'what is your favourate pizza flavour', a: 'hawaiian' }
  ]

  const profileId = '%FiR41bB1CrsanZA3VgAzoMmHEOl8ZNXWn+GS5vW3E/8=.sha256'
  const recps = [poBoxId, server.id]

  let id, val
  try {
    id = await p(server.registration.tribe.create)(groupId, { answers, profileId, recps })
    val = await p(server.get)({ id, private: true })
  } catch (err) {
    t.fail(err)
  }

  t.deepEqual(
    val.content,
    {
      type: 'registration/group',
      groupId,
      version: '1',
      answers: { set: answers },
      profileId,
      tangles: {
        registration: { root: null, previous: null }
      },
      recps
    },
    'publishes correct message'
  )

  server.close()
  t.end()
})
