const { SecretKey } = require('ssb-private-group-keys')
const { replicate } = require('scuttle-testbot')
const bfe = require('ssb-bfe')
const keys = require('ssb-keys')
const crypto = require('crypto')

const Server = require('./test-bot')

module.exports = {
  mock: {
    groupId: () => `%${new SecretKey().toString()}.cloaked`,
    feedId: () => keys.generate().id,
    poBoxId: () => bfe.decode(
      Buffer.concat([
        bfe.toTF('identity', 'po-box'),
        crypto.randomBytes(32)
      ])
    ),
    msgId: () => bfe.decode(
      Buffer.concat([
        bfe.toTF('message', 'classic'),
        crypto.randomBytes(32)
      ])
    )
  },
  replicate,
  Server
}
