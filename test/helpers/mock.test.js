const test = require('tape')
const { mock } = require('./')

test('helpers.mock', t => {
  let value
  for (const type in mock) {
    value = mock[type]()
    t.true(value, `${type}: ${value}`)
  }

  t.end()
})
