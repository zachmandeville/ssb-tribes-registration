# ssb-tribes-registration

## Example Usage

```js
const Stack = require('secret-stack')
const caps = require('ssb-caps')

const stack = Stack({ caps })
  .use(require('ssb-backlinks'))
  .use(require('ssb-query'))
  .use(require('ssb-tribes'))
  .use(require('ssb-tribes-registration'))

const ssb = stack()

const details = {
  profileId, // an ssb-profile
  answers: [
    {
      question: 'where were you born?',
      answer: 'the heretaunga plains'
    }
  ],
  recps: [poBoxId, ssb.id]
}

ssb.registration.tribe.create(groupId, details, (err, registrationId) => {
  // ...

})
```

## API

### `ssb.registration.tribe.create(groupdId, opts, cb)`

Creates a tribe registration message directed to the administrators of a
private-group.

where:

- `opts` *Object*:
  - `opts.profileId` *String* - the id of a `profile/*` record
      - if `opts.recps` is provided, this profile must be either public or also
        encrypted to `opts.recps`
  - `opts.answers` *Array* - a collection of questions and their answers of form
    `[{ q: String, a: String }, ...]`
  - `opts.comment` *String* - text to be viewed by the Kaitiakis of a group
  - `opts.recps` *Array* - depending on your encryption plugins entries could be
    `FeedId|GroupId|POBoxId` etc.

> These are optional and can be left out using:
> `ssb.registration.tribe.create(groupId, cb)` if no opts are needed


Calls back with a `registrationId` (the messageId of the root message of this
registrations tangle)

### `ssb.registration.tribe.accept(registrationId, opts, cb)`

This runs `ssb.tribes.invite` to invite the person to the group, then publishes
an update to the registration linking to that message.

where:
- `opts` *Object*:
  - `opts.comment` *String*: a message to be viewed in the registration thread
    along with the acceptant
  - `opts.groupIntro` *String*: a message that will be published along with the
    `group/add-member` message


### `ssb.registration.tribe.reject(registrationId, opts, cb)`

where:
- `opts` *Object*:
  - `opts.reason` *String*: a message to be viewed in the registration thread
    along with the rejection


### `ssb.registration.tribe.get(registrationId, cb)`

Returns the current state of a tribe registration. e.g.

```js
{
  id: '%CXVDe5AoPVf83CoHYBpfplpzTU/YYkN56yge1nBG9wE=.sha256',
  groupId: '%A9OUzXtv7BhaAfSMqBzOO6JC8kvwmZWGVxHDAlM+/so=.cloaked',
  applicantId: '@35wu1YDBx9NPsUXpe7bYmHb7BQFEfn2ZFh0DZ6OipA0=.ed25519', // feedId of the applicant
  profileId: '%FiR41bB1CrsanZA3VgAzoMmHEOl8ZNXWn+GS5vW3E/8=.sha256',
  recps: [
    '@CQi7RZDHLHalHErknddXIczj6FulnAdbYfULVSXTbns=.ed25519',
    '@qYeVniXyC0/D9GIlGMAiIKg5jGgJTY7ZEgeikRWIJ/Y=.ed25519',
  ],

  answers: [
    {
      q: 'where are you from?',
      a: 'I was born in Hawkes Bay'
    }
  ],
  decision: {
    accepted: true,
    addMember: '%pfplpzTU/YYkN56yge1CXVDe5AoPVf83CoHYBnBG9wE=.sha256'
    // link to message which added them to group
  },

  history: [
    {
      type: 'answers',
      author: '@35wu1YDBx9NPsUXpe7bYmHb7BQFEfn2ZFh0DZ6OipA0=.ed25519', // registrationId
      timestamp: 1613003009958,
      body: [
        {
          q: 'where are you from?',
          a: 'I was born in Hawkes Bay'
        }
      ]
    },
    {
      type: 'comment',
      author: kaitiaki.id,
      timestamp: 1613003010973,
      body: 'WELCOME!'
    },
    {
      type: 'decision',
      author: '@CQi7RZDHLHalHErknddXIczj6FulnAdbYfULVSXTbns=.ed25519', // groupAdmins[0]
      timestamp: 1613003010973,
      body: {
       accepted: true,
       addMember: '%pfplpzTU/YYkN56yge1CXVDe5AoPVf83CoHYBnBG9wE=.sha256'
      }
    }
  ]
}
```

NOTE:
- if two decisions are concurrently posted by two different admins, the
  `decision` field shows:
    - the latest acceptance decision
    - OR if there's been no acceptance the latest rejection (by authored
      timestamp)

### `ssb.registration.tribe.list(cb)`

where `cb` calls back with an Array of registration ids.

Alternatively, you can call with opts `ssb.registration.tribe.list(opts, cb)`
where `opts` *Object* with properties:
    - `opts.groupId` *MessagedId*: return only registration for a specific group
    - `opts.get` *Function | true* - runs an async function on each
      registrationId before calling back. If `true` is passed, this is the
      internal `ssb.registration.tribe.get`.
    - `opts.accepted` *(Boolean|null)*: filter registrations that have been
      accepted or not.
        - `accepted: true` gets you registrations which have been accepted
        - `accepted: false` gets you registrations which have been rejected
        - `accepted: null` gets you registrations which haven't had a decision
          made on them yet
        - `accepted: undefined` gets you all registrations
        - If you set this, you get full registration records back

